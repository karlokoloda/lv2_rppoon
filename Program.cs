﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2_Koloda
{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller diceRoller = new DiceRoller();
            FlexibleDiceRoller flexibleDiceRoller = new FlexibleDiceRoller();
            ClosedDiceRoller closedDiceRoller = new ClosedDiceRoller(5, 6);

            ConsoleLogger logDiceRoller = new ConsoleLogger();
            logDiceRoller.Log(diceRoller);
            FileLogger fileLogger = new FileLogger("./logDiceRoller");
            fileLogger.Log(diceRoller);

            ConsoleLogger logFlexibleDiceRoller = new ConsoleLogger();
            logFlexibleDiceRoller.Log(diceRoller);
            FileLogger fileLogFlexibleDiceRoller = new FileLogger("./logFlexibleDiceRoller");
            fileLogFlexibleDiceRoller.Log(flexibleDiceRoller);

            ConsoleLogger logClosedDiceRoller = new ConsoleLogger();
            logClosedDiceRoller.Log(diceRoller);
            FileLogger fileLogClosedDiceRoller = new FileLogger("./logClosedDiceRoller");
            fileLogClosedDiceRoller.Log(closedDiceRoller);


            const int numberOfDice = 20;

            // Zadatak 2
            //Random randomGenerator = new Random();

            //Zadatak 1. i 3. 
            for (int i = 0; i < numberOfDice; i++)
            {
                flexibleDiceRoller.InsertDie(new Die(6));
            }
            //

            for (int i = 0; i < numberOfDice; i++)
            {
                diceRoller.InsertDie(new Die(6));
            }

            //Zadatak 2 - izmjena - dodavanje randomGenerator
            /*
            for (int i = 0; i < numberOfDice; i++)
            {
                diceRoller.InsertDie(new Die(6, randomGenerator));
            }
            */


            diceRoller.RollAllDice();
            flexibleDiceRoller.RollAllDice();
            closedDiceRoller.RollAllDice();
            Console.WriteLine(diceRoller.DiceCount);
            Console.WriteLine(flexibleDiceRoller.DiceCount);
            Console.WriteLine(closedDiceRoller.DiceCount);
            flexibleDiceRoller.RemoveAllDice();

            IList<int> rolls = diceRoller.GetRollingResults();
            foreach (int result in rolls)
            {
                Console.WriteLine(result);
            }
            IList<int> rolls2 = flexibleDiceRoller.GetRollingResults();
            foreach (int result in rolls2)
            {
                Console.WriteLine(result);
            }
            IList<int> rolls3 = closedDiceRoller.GetRollingResults();
            foreach (int result in rolls3)
            {
                Console.WriteLine(result);
            }

            flexibleDiceRoller.RemoveDiceWithSides(6);
            Console.WriteLine(flexibleDiceRoller.DiceCount);
        }
    }
}
