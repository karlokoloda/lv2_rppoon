﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2_Koloda
{
    interface IHandleDice
    {
        void RemoveAllDice();
        void RollAllDice();
    }
}
