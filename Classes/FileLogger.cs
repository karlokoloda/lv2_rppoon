﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2_Koloda
{
    class FileLogger : ILogger
    {
        private string filePath;
        public FileLogger(string filePath)
        {
            this.filePath = filePath;
        }
        //Zadatak 4.
        /*
        public void Log(string message)
        {
            using (System.IO.StreamWriter fileWriter =
            new System.IO.StreamWriter(this.filePath, true))

            {
                fileWriter.WriteLine(message);
            }
        }
        */

        //Zadatak 5. 
        public void Log(ILogable data)
        {
            using (System.IO.StreamWriter fileWriter =
            new System.IO.StreamWriter(this.filePath))

            {
                fileWriter.WriteLine(data.GetStringRepresentation());
            }
        }
    }
}
