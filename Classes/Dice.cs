﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2_Koloda
{
    class Die
    {
        private int numberOfSides;
        //Zadatak 1. i 2. 
        //private Random randomGenerator;

        //Zadatak 3. 
        private RandomGenerator randomGenerator;
        private int rolledNumber;

        //Zadatak 1
        /*
        public Die(int numberOfSides) 
        { 
            this.numberOfSides = numberOfSides;
            this.randomGenerator = new Random();
        }
        */

        //Zadatak 2 - dodavanje randomGenerator kao parametar
        /*
        public Die(int numberOfSides, Random randomGenerated)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = randomGenerated;
        }
        */

        //Zadatak 3 - izmjena 1. - dodavanje Singleton
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = RandomGenerator.GetInstance();
        }

        public int Roll() 
        {
            //zadatak 1. i 2.
            //rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
            //Zadatak 3. 
            rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1);

            return rolledNumber; 
        }

        //7. Zadatak
        public int NumberOfSides
        {
            get { return this.numberOfSides; }
        }
    }
}
