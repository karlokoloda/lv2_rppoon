﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2_Koloda
{
	class DiceRoller : ILogable
	{

		private List<Die> dice;
		private List<int> resultForEachRoll;
		//Zadatak 4. 
		//private ILogger logger;

		public DiceRoller()
		{
			this.dice = new List<Die>();
			this.resultForEachRoll = new List<int>();
		}

		public void InsertDie(Die die)
		{
			dice.Add(die);
		}

		public void RollAllDice()
		{//clear results of previous rolling
			this.resultForEachRoll.Clear();
			foreach (Die die in dice)
			{
				this.resultForEachRoll.Add(die.Roll());
			}
		}

		//View of the results
		public IList<int> GetRollingResults()
		{
			return new System.Collections.ObjectModel.ReadOnlyCollection<int>(this.resultForEachRoll);
		}

		public int DiceCount
		{
			get { return dice.Count; }
		}

		//Zadatak 4.
		/*public void SetLogger(ILogger logger)
		{
			this.logger = logger;
		}
		public void LogResults()
		{
			foreach (int result in this.resultForEachRoll)
			{
				logger.Log(result.ToString());
			}
		}
		*/

		//Zadatak 5.
		public string GetStringRepresentation()
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (int result in this.resultForEachRoll)
			{
				stringBuilder.Append(result.ToString()).Append("\n");
			}
			return stringBuilder.ToString();
		}
	}
}
