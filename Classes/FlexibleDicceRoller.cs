﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2_Koloda
{
    class FlexibleDiceRoller : IRoller, IHandleDice, ILogable
    {
        private List<Die> dice; 
        private List<int> resultForEachRoll;

        public FlexibleDiceRoller() 
        { 
            this.dice = new List<Die>(); 
            this.resultForEachRoll = new List<int>(); 
        }

        public void InsertDie(Die die) 
        { 
            dice.Add(die); 
        }

        public void RemoveAllDice() 
        { 
            this.dice.Clear(); 
            this.resultForEachRoll.Clear(); 
        }

        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(this.resultForEachRoll);
        }

        public int DiceCount
        {
            get { return dice.Count; }
        }

        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }

        //Zadatak 7.
        public void RemoveDiceWithSides(int sides)
        {
            for (int i = 0; i < dice.Count; i++)
            {
                if (dice[i].NumberOfSides == sides)
                {
                    dice.RemoveAt(i);

                    --i;
                }
            }
        }

        public string GetStringRepresentation()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (int result in this.resultForEachRoll)
            {
                stringBuilder.Append(result.ToString()).Append("\n");
            }
            return stringBuilder.ToString();
        }
    }
}
