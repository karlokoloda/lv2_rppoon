﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2_Koloda
{
    class ConsoleLogger : ILogger
    {
        //Zadatak 4.
        /*
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
        */

        //Zadatak 5.
        public void Log(ILogable data)
        {
            Console.WriteLine(data.GetStringRepresentation());
        }
    }
}
